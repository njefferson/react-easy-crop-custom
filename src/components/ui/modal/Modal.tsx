import { useGlobalContext } from "@/components/contexts/cropImageContext";
import Crop from "@/components/layouts/cropper";
import React from "react";
import { FiCrop, FiX } from "react-icons/fi";
import { motion, AnimatePresence } from "framer-motion"

interface ModalProps {
  open: boolean;
  onClose: () => void;
}

const Modal: React.FC<ModalProps> = ({ open, onClose }) => {
    const { 
        showCroppedImage, 
        zoom, 
        setZoom,
        aspect,
        setAspect,
    } = useGlobalContext();

    const doneCropHandling = () => {
        onClose();
        showCroppedImage();
    };

  return (
    <AnimatePresence mode="wait">
        {open && (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.3 }}
                onClick={onClose}
                className="fixed top-0 left-0 h-screen w-full bg-gray-700/70 backdrop-blur flex items-center justify-center z-[1000]"
            >
                <motion.div
                    initial={{ scale: 0.8, opacity: 0 }}
                    animate={{ scale: 1, opacity: 1 }}
                    exit={{ scale: 0.8, opacity: 0 }}
                    transition={{ duration: 0.3 }}
                    onClick={(e) => e.stopPropagation()} 
                    className="w-[50rem] shadow-xl bg-white rounded-[1.6rem] p-[2rem] relative"
                >
                    <div className="flex items-start justify-between">
                    <div className="flex items-center gap-[0.8rem]">
                        <div className="h-[4.8rem] w-[4.8rem] rounded-[0.8rem] border border-gray-200 flex items-center justify-center">
                        <FiCrop className="text-black text-[2.2rem]" />
                        </div>
                        <div className="space-y-[0.4rem]">
                        <h3 className="text-[1.8rem] leading-[2.8rem] font-[600] text-black">
                            {"Change profile image"}
                        </h3>
                        <span className="text-[1.4rem] leading-[2.4rem] font-[400] text-gray-400">
                            {"Format image png, jpg and jpeg"}
                        </span>
                        </div>
                    </div>
                    <div onClick={onClose} className="cursor-pointer hover:rotate-180 transition-all duration-300 ease-out">
                        <FiX className="text-black text-[2.2rem]" />
                    </div>
                    </div>
                    
                    {/* cropper */}
                    <div className="h-[30rem] w-full border border-dashed border-gray-200 rounded-[1.6rem] bg-gray-100 my-[2rem] relative overflow-hidden">
                        <Crop />
                    </div>

                    {/* input type range zoom and crop */}
                    <div className="flex flex-col mb-[2rem]">
                        <span className="text-[1.4rem] leading-[2.4rem] font-[500] text-black block mb-2">{"Zoom"}</span>
                        <input
                            type="range"
                            value={zoom}
                            min={1}
                            max={3}
                            step={0.1}
                            aria-labelledby="Zoom"
                            onChange={(e) => {
                                setZoom(e.target.value)
                            }}
                            className="w-full h-2 mb-6 bg-violet-100 rounded-lg appearance-none cursor-pointer dark:bg-violet-100"
                        />
                    </div>
                    <div className="flex flex-col mb-[2rem]">
                        <span className="text-[1.4rem] leading-[2.4rem] font-[500] text-black block mb-2">{"Cadrage"}</span>
                        <input
                            type="range"
                            value={aspect}
                            min={0}
                            max={3}
                            step={0.1}
                            aria-labelledby="Aspect"
                            onChange={(e) =>  {
                                setAspect(parseFloat(e.target.value))
                            }}
                            className="w-full h-2 mb-6 bg-violet-100 rounded-lg appearance-none cursor-pointer dark:bg-violet-100"
                        />
                    </div>

                    {/* button action */}
                    <div className="flex items-center justify-between gap-[1.6rem] mt-[2rem]">
                        <button 
                            onClick={onClose} 
                            type="button" className="px-[2rem] py-[1rem] rounded-[1.6rem] border border-gray-300 w-full"
                        >
                            <span className="text-[1.4rem] leading-[2.4rem] text-black font-[500]">{"Cancel"}</span>
                        </button>
                        <button 
                                onClick={doneCropHandling} 
                                className="px-[2rem] py-[1rem] rounded-[1.6rem] bg-black border border-black w-full"
                            >
                            <span className="text-[1.4rem] leading-[2.4rem] text-white font-[500]">{"Save changes"}</span>
                        </button>
                    </div>
                </motion.div>
            </motion.div>
        )}
    </AnimatePresence>
  );
};

export default Modal;