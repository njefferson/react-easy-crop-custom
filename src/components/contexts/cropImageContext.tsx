import React, { useCallback, useContext, useState } from "react";
import getCroppedImg from "../utils/cropImage";

const AppContext = React.createContext<any>(null);

interface AppProviderProps {
  children: React.ReactNode;
}

const AppProvider: React.FC<AppProviderProps> = ({ children }) => {
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState<any>(null);
    const [zoom, setZoom] = useState<number>(1);
    const [crop, setCrop] = useState<{ x: number; y: number }>({ x: 0, y: 0 });
    const [rotation, setRotation] = useState<number>(0);
    const [croppedImage, setCroppedImage] = useState<string | null>(null);
    const [imageSrc, setImageSrc] = useState<string | null>(null);
    const [aspect, setAspect] = useState(1);

  const onCropComplete = useCallback((croppedArea: any, croppedAreaPixels: any) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  const showCroppedImage = useCallback(async () => {
    try {
      if (imageSrc && croppedAreaPixels) {
        const croppedImage = await getCroppedImg(imageSrc, croppedAreaPixels, rotation);
        setCroppedImage(croppedImage);
      }
    } catch (e) {
      console.error(e);
    }
  }, [imageSrc, croppedAreaPixels, rotation]);


  const onFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const emptyArray: any = []
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      const imageDataUrl: string | null = await readFile(file);

      setImageSrc(imageDataUrl);
      setModalOpen(true);

      e.target.value =""
    }
  };

  const readFile = (file: File) => {
    return new Promise<string | null>((resolve) => {
      const reader = new FileReader();
      reader.addEventListener("load", () => resolve(reader.result as string));
      reader.readAsDataURL(file);
    });
  };

  return (
    <AppContext.Provider
      value={{
        aspect,
        setAspect,
        zoom,
        setZoom,
        crop,
        setCrop,
        rotation,
        setRotation,
        croppedAreaPixels,
        setCroppedAreaPixels,
        croppedImage,
        setCroppedImage,
        onCropComplete,
        showCroppedImage,
        onFileChange,
        imageSrc,
        modalOpen,
        setModalOpen,
        setImageSrc,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };
