"use client"

import Image from "next/image"
import { useRef } from "react"
import { useGlobalContext } from "@/components/contexts/cropImageContext"
import Modal from "@/components/ui/modal/Modal"

export default function ImageCrop() {

    const {
        croppedImage,
        onFileChange,
        modalOpen,
        setModalOpen,
    } = useGlobalContext();
    
    const fileInoutRef = useRef();

    const toggleModal = () => {
      setModalOpen(!modalOpen);
    };

  
    return (
        <>
            <div className="h-screen flex flex-col items-center justify-center">
                <h2 className="text-[3rem] leading-[4rem] font-[700] text-center text-black">{'Profile Crop In NextJs'}</h2>
                <div className="py-[6rem]">
                    <div onClick={toggleModal} className="flex items-center gap-[1.2rem] cursor-pointer">
                        {croppedImage ? (
                            <div className="border-2 border-dashed border-violet-500 h-[9rem] w-[9rem] rounded-full flex items-center justify-center shrink-0">
                                <div className="h-[8rem] w-[8rem] relative rounded-full overflow-hidden">
                                    <Image 
                                        alt="profile"
                                        src={croppedImage} 
                                        fill 
                                        className="object-cover" 
                                    />
                                </div>
                            </div>
                        ) : (
                            <label htmlFor="upload-image">
                                <div  className="h-[8rem] w-[8rem] relative rounded-full overflow-hidden shrink-0">
                                    <Image
                                        alt="upload-placeholder"
                                        src="/assets/images/avatar.svg"
                                        fill 
                                        className="object-cover"
                                    />
                                </div>
                            </label>
                        )}
                        <div className="flex flex-col space-y-[1rem]">
                            <span className="text-[1.8rem] leading-[2.8rem] font-[500] text-violet-500">{"John Doe Smith"}</span>
                            <label 
                                htmlFor="upload-image"
                                className="px-[2rem] py-[0.4rem] rounded-[1rem] bg-gray-100 hover:bg-violet-500 hover:text-white text-black w-full text-center transition-all duration-300 ease-out cursor-pointer"
                            >
                                <input
                                    onChange={onFileChange}
                                    type="file"
                                    className="hidden"
                                    id="upload-image"
                                    ref={(el: any)=> fileInoutRef.current = el}
                                />
                                <span className="text-[1.4rem] leading-[2.4rem] font-[500]">{"Update profile"}</span>
                            </label>
                        </div>
                    </div>
                    {/* <div>
                        <button
                        className="bg-blue-500 p-3 text-white rounded-xl m-5 hover:bg-green-500"
                        onClick={() => setModalOpen(true)}
                        >
                        edit cropping
                        </button>
                        <button
                        className="bg-blue-500 p-3 text-white rounded-xl m-5 hover:bg-green-500"
                        onClick={() => setCroppedImage(null)}
                        >
                        Clear Image
                        </button>
                    </div> */}
                </div>
            </div>

            {/* modal */}
            <Modal 
                open={modalOpen} 
                onClose={toggleModal}
            />
        </>
    )
}