import React from "react";
import Cropper from "react-easy-crop";
import { useGlobalContext } from "../contexts/cropImageContext";

export default function Crop() {
  const {
    aspect,
    zoom,
    setZoom,
    crop,
    setCrop,
    rotation,
    setRotation,
    onCropComplete,
    imageSrc
  } = useGlobalContext();

  return (
    <div className="">
      <div>
        <Cropper
          image={imageSrc}
          crop={crop}
          rotation={rotation}
          zoom={zoom}
          aspect={aspect}
          onCropChange={setCrop}
          onRotationChange={setRotation}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
        />
      </div>
    </div>
  );
}
