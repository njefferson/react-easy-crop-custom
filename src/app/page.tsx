'use client'
import ImageCrop from '@/components/sections/crop/ImageCrop'

export default function Home() {
  
  return (
    <div>
      <ImageCrop />
    </div>
  )
}
