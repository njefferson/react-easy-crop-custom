"use client"

import { AppProvider } from '@/components/contexts/cropImageContext'
import './globals.css'
// import type { Metadata } from 'next'
import { Onest } from 'next/font/google'

const onest = Onest({
  weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900'],
  subsets: ['latin'],
  display: 'swap',
})


// export const metadata: Metadata = {
//   title: 'Create Next App',
//   description: 'Generated by create next app',
// }

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {

  return (
    <html lang="en">
      <body className={onest.className}>
        <AppProvider>
          {children}
        </AppProvider>
      </body>
    </html>
  )
}
